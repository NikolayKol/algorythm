package com.company.data;

import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.NotPositiveException;
import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.linear.RealVector;

import java.util.Arrays;

/**
 * Created by nickolay on 06.06.16.
 */
public class Vect extends RealVector {

    private double[] vect;

    public Vect(double[] vect) {
        this.vect = vect;
    }

    @Override
    public int getDimension() {
        return vect.length;
    }

    @Override
    public double getEntry(int i) throws OutOfRangeException {
        return vect[i];
    }

    @Override
    public void setEntry(int i, double v) throws OutOfRangeException {
        vect[i] = v;
    }

    @Override
    public RealVector append(RealVector realVector) {
        return null;
    }

    @Override
    public RealVector append(double v) {
        return null;
    }

    @Override
    public RealVector getSubVector(int i, int i1) throws NotPositiveException, OutOfRangeException {
        return null;
    }

    @Override
    public void setSubVector(int i, RealVector realVector) throws OutOfRangeException {

    }

    @Override
    public boolean isNaN() {
        return false;
    }

    @Override
    public boolean isInfinite() {
        return false;
    }

    @Override
    public RealVector copy() {
        return new Vect(Arrays.copyOf(vect, vect.length));
    }

    @Override
    public RealVector ebeDivide(RealVector realVector) throws DimensionMismatchException {
        return null;
    }

    @Override
    public RealVector ebeMultiply(RealVector realVector) throws DimensionMismatchException {
        return null;
    }

    public double[] getData() {
        return vect;
    }
}
