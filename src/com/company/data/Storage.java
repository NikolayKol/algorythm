package com.company.data;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.random.CorrelatedRandomVectorGenerator;
import org.apache.commons.math3.random.GaussianRandomGenerator;
import org.apache.commons.math3.random.JDKRandomGenerator;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.stat.correlation.Covariance;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by nickolay on 06.06.16.
 */

public class Storage {

    private static Storage instance;

    private List<Vect> vectors;

    private RandomGenerator rg;
    private GaussianRandomGenerator rawGenerator;
    private CorrelatedRandomVectorGenerator generator;

    private double[] mean;
    private RealMatrix covariance;

    private final double small = 1.0e-18;

    private Storage() {

        // Create and seed a RandomGenerator (could use any of the generators in the random package here)
        rg = new JDKRandomGenerator();
        rg.setSeed(17399225432l);  // Fixed seed means same results every time

        // Create a GassianRandomGenerator using rg as its source of randomness
        rawGenerator = new GaussianRandomGenerator(rg);

        init();

        // Create a CorrelatedRandomVectorGenerator using rawGenerator for the components
        generator = new CorrelatedRandomVectorGenerator(mean, covariance, small * covariance.getNorm(), rawGenerator);

    }

    public static Storage getInstance() {
        if (instance == null) instance = new Storage();

        return instance;
    }

    public long generateData() {

        long time = System.currentTimeMillis();

        vectors = new ArrayList<>();

        for (int i = 0; i < 200000; i++) {
            Vect vect = new Vect(generator.nextVector());
            vectors.add(vect);
        }

        return System.currentTimeMillis() - time;
    }

    private long searchL2(final Vect random) {

        long time = System.currentTimeMillis();

        List<Vect> closest = new ArrayList<>(10);

        for (int i = 0; i < 10; i++) {
            closest.add(vectors.get(i));
        }

        Collections.sort(closest, new Comparator<Vect>() {
            @Override
            public int compare(Vect vect, Vect t1) {
                double distance = random.getDistance(vect) - random.getDistance(t1);

                if (distance > 0) return 1;
                else if (distance < 0) return -1;
                else return 0;

            }
        });

        double maxDistanceOfClosest = distanseByL2(random, closest.get(9));

        for (int i = 10; i < vectors.size(); i++) {

            Vect vect = vectors.get(i);

            if (distanseByL2(random, vect) < maxDistanceOfClosest) {

                closest.add(9, vect);

                Collections.sort(closest, new Comparator<Vect>() {
                    @Override
                    public int compare(Vect vect, Vect t1) {
                        double distance = random.getDistance(vect) - random.getDistance(t1);

                        if (distance > 0) return 1;
                        else if (distance < 0) return -1;
                        else return 0;

                    }
                });

                maxDistanceOfClosest = distanseByL2(random, closest.get(9));
            }
        }

        return System.currentTimeMillis() - time;
    }

    private long searchMahalanobis(final Vect random) {

        long time = System.currentTimeMillis();

        List<Vect> closest = new ArrayList<>(10);

        for (int i = 0; i < 10; i++) {
            closest.add(vectors.get(i));
        }

        Collections.sort(closest, new Comparator<Vect>() {
            @Override
            public int compare(Vect vect, Vect t1) {
                double distance =
                        distanceByMahalanobis(covariance, random, vect) - distanceByMahalanobis(covariance, random, t1);

                if (distance > 0) return 1;
                else if (distance < 0) return -1;
                else return 0;
            }
        });

        double maxDistanceOfClosest = distanceByMahalanobis(covariance, random, closest.get(9));

        for (int i = 10; i < vectors.size(); i++) {

            Vect vect = vectors.get(i);

            if (distanceByMahalanobis(covariance, random, vect) < maxDistanceOfClosest) {

                closest.add(9, vect);

                Collections.sort(closest, new Comparator<Vect>() {
                    @Override
                    public int compare(Vect vect, Vect t1) {
                        double distance =
                                distanceByMahalanobis(covariance, random, vect) - distanceByMahalanobis(covariance, random, t1);

                        if (distance > 0) return 1;
                        else if (distance < 0) return -1;
                        else return 0;
                    }
                });

                maxDistanceOfClosest = distanceByMahalanobis(covariance, random, closest.get(9));
            }

//            System.out.println(i);
        }

        return System.currentTimeMillis() - time;
    }

    public long findRandomVectByL2() {
        Vect randomVect = new Vect(generator.nextVector());
        return searchL2(randomVect);
    }

    public long findRandomVectByMahalanobis() {
        Vect randomVect = new Vect(generator.nextVector());
        return searchMahalanobis(randomVect);
    }

    private double distanceByMahalanobis(RealMatrix m1, Vect vect0, Vect vect1) {

        double v1[] = vect0.getData();
        double v2[] = vect1.getData();


        double det = Math.pow((new LUDecomposition(m1).getDeterminant()), 1/(v1.length));
        double[] tempSub = new double[v1.length];
        for(int i=0; i < v1.length; i++){
            tempSub[i] = (v1[i]-v2[i]);
        }
        double[] temp = new double[v1.length];
        for(int i=0; i < temp.length; i++){
            temp[i] = tempSub[i]*det;
        }
        RealMatrix m2 = new Array2DRowRealMatrix(new double[][] { temp });
        RealMatrix m3 = m2.multiply(new LUDecomposition(m1, small * m1.getNorm()).getSolver().getInverse());
        RealMatrix m4 = m3.multiply((new Array2DRowRealMatrix(new double[][] { temp })).transpose());
        double distance = Math.sqrt(m4.getEntry(0, 0));
        return distance;
    }

    private double distanseByL2(Vect vect0, Vect vect1) {
        return vect0.getDistance(vect1);
    }

    private void init() {

        double[] mean = new double[250];
        double[][] cov = new double[250][250];

        for (int i = 0; i < 250; i++) {
            mean[i] = Math.abs(rg.nextDouble() * 6);

            for (int j = 0; j < 250; j++) {
                cov[i][j] = Math.abs(rg.nextDouble() * 6);
            }
        }

        for (int i = 0; i < 250; i++) {
            for (int j = 0; j < 250; j++) {
                if (cov[i][j] < 0) {
                    System.out.println("less than 0");
                }
            }
//            cov[i][249 - i] = 6;
        }

        Covariance covariance = new Covariance(cov);

        this.covariance = covariance.getCovarianceMatrix();
        this.mean = mean;

    }
}
