package com.company.data;

import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.RealMatrix;

/**
 * Created by nickolay on 06.06.16.
 */
public class LUDecompositionImpl extends LUDecomposition {

    public LUDecompositionImpl(RealMatrix matrix) {
        super(matrix);
    }

    public LUDecompositionImpl(RealMatrix matrix, double singularityThreshold) {
        super(matrix, singularityThreshold);
    }
}
