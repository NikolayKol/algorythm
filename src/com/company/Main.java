package com.company;

import com.company.data.Storage;

import java.util.Scanner;


public class Main {

    static Storage storage;

    public static void main(String[] args) {

        storage = Storage.getInstance();
        System.out.println("please wait, data creating");
        long generatingTime = storage.generateData();
        System.out.println("generating completes in " + generatingTime + "ms");

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("press 'q' for exit, 'l' for find by L2, and 'm' for find by Mahalanobis");

            String command = scanner.next();
            if (command.contains("q")) break;

            if (command.contains("l")) {
                long findingTime = storage.findRandomVectByL2();
                System.out.println("finding completes in " + findingTime + " ms");
            } else if (command.contains("m")) {
                long findingTime = storage.findRandomVectByMahalanobis();
                System.out.println("finding completes in " + findingTime + " ms");
            }
        }
    }
}
